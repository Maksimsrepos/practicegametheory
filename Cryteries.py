from random import randint
from copy import deepcopy
from operator import xor

class Methods():
    def method_valds(self,ilist):
        print("Критерий Вальда(максиминный):")
        compare = []
        stratnum = ""
        i = 0
        for row in ilist:
            stratnum = "A"+str(i)
            minval = min(row), stratnum #выбираем минимальное значение из строки
            compare.append(minval)
            i+= 1
        print ("optimal stategy is " + str(max(compare)))

    def method_maxi(self, ilist):
        print("Критерий максимума")
        compare = []
        stratnum = ""
        i = 0
        for row in ilist:
            stratnum = "A"+str(i)
            maxval = max(row), stratnum #выбираем максимальное значение из строки
            compare.append(maxval)
            i+=1
        maxfromall = max(compare)
        print ("optimal stategy is " + str(maxfromall))

    def method_gurvitz(self, ilist):
        print("Критерий Гурвица")
        compare = []
        stratnum = ""
        i = 0
        A = 0.6 # поскольку величина этого индекса означает степень ответственности игрока, возьмем величину отличную от 0 или 1 дабы были отличия от метод Вальда и максимума
        for row in ilist:
            stratnum = "A"+str(i)
            maxval = max(row) #выбираем максимальное значение из строки
            minval = min(row) #выбираем минимальное значение из строки
            result = (A*maxval+ (1-A)*minval), stratnum
            compare.append(result)
            i+=1
        maxfromall = max(compare)
        print ("optimal stategy is " + str(maxfromall))

    def method_sevidzh(self, ilist):
        print("Критерий Сэвиджа(минимаксный)")
        compare = []
        stratnum = ""
        i = 0
        for row in ilist:
            stratnum = "A"+str(i)
            maxval = max(row), stratnum #выбираем минимальное значение из строки
            compare.append(maxval)
            i+=1
        minfromall = min(compare)
        print ("optimal stategy is " + str(minfromall))

    def method_bayes(self, ilist):
        print("Критерий Байеса")
        compare = []
        stratnum = ""
        i = 0
        for row in ilist:
            stratnum = "A"+str(i)
            i+=1
            rowsum = (1 / len(row)) * sum(row), stratnum  #принимаем что вероятности одинаковы для всех случаев, это значит что общий множитель можно вынести за скобку
            compare.append(rowsum)
        print ("optimal stategy is " + str(max(compare)))
        

